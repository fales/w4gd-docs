:github_url: hide

.. DO NOT EDIT THIS FILE!!!
.. Generated automatically from W4GD sources.
.. GDScript source: https://gitlab.com/W4Games/w4gd/-/blob/main/game_server/game_server.gd.

.. _class_addons_w4gd_game_server_game_server_gd_statechangerequest:

res://addons/w4gd/game_server/game_server.gd:StateChangeRequest
===============================================================

**Inherits:** RefCounted

Internal class used to coordinate state changes with Agones.

.. rst-class:: classref-reftable-group

Methods
-------

.. table::
   :widths: auto

   +------+------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`cancel<class_addons_w4gd_game_server_game_server_gd_statechangerequest_method_cancel>` **(** **)**                           |
   +------+------------------------------------------------------------------------------------------------------------------------------------+
   | bool | :ref:`execute<class_addons_w4gd_game_server_game_server_gd_statechangerequest_method_execute>` **(** **)**                         |
   +------+------------------------------------------------------------------------------------------------------------------------------------+
   | int  | :ref:`get_requested_state<class_addons_w4gd_game_server_game_server_gd_statechangerequest_method_get_requested_state>` **(** **)** |
   +------+------------------------------------------------------------------------------------------------------------------------------------+
   | bool | :ref:`needs_retry<class_addons_w4gd_game_server_game_server_gd_statechangerequest_method_needs_retry>` **(** **)**                 |
   +------+------------------------------------------------------------------------------------------------------------------------------------+

.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Signals
-------

.. _class_addons_w4gd_game_server_game_server_gd_statechangerequest_signal_completed:

.. rst-class:: classref-signal

**completed** **(** Variant requested_state **)**

.. container:: contribute

	There is currently no description for this signal.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_game_server_game_server_gd_statechangerequest_signal_failed:

.. rst-class:: classref-signal

**failed** **(** Variant requested_state **)**

.. container:: contribute

	There is currently no description for this signal.

.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Method Descriptions
-------------------

.. _class_addons_w4gd_game_server_game_server_gd_statechangerequest_method_cancel:

.. rst-class:: classref-method

void **cancel** **(** **)**

.. container:: contribute

	There is currently no description for this method.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_game_server_game_server_gd_statechangerequest_method_execute:

.. rst-class:: classref-method

bool **execute** **(** **)**

.. container:: contribute

	There is currently no description for this method.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_game_server_game_server_gd_statechangerequest_method_get_requested_state:

.. rst-class:: classref-method

int **get_requested_state** **(** **)**

.. container:: contribute

	There is currently no description for this method.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_game_server_game_server_gd_statechangerequest_method_needs_retry:

.. rst-class:: classref-method

bool **needs_retry** **(** **)**

.. container:: contribute

	There is currently no description for this method.

.. |virtual| replace:: :abbr:`virtual (This method should typically be overridden by the user to have any effect.)`
.. |const| replace:: :abbr:`const (This method has no side effects. It doesn't modify any of the instance's member variables.)`
.. |vararg| replace:: :abbr:`vararg (This method accepts any number of arguments after the ones described here.)`
.. |constructor| replace:: :abbr:`constructor (This method is used to construct a type.)`
.. |static| replace:: :abbr:`static (This method doesn't need an instance to be called, so it can be called directly using the class name.)`
.. |operator| replace:: :abbr:`operator (This method describes a valid operator to use with this type as left-hand operand.)`
