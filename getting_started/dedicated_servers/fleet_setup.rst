.. _doc_getting_started_dedicated_servers_fleet_setup:

Creating a fleet
================

A "Fleet" is a collection of Game Servers running the same game build and configuration.

You can create one in the W4 Cloud dashboard by:

#. Going to Multiplayer -> Fleets

   .. image:: img/fleets_menu_item.png

#. Clicking the "+ New Fleeet" button in the upper-right corner

   .. image:: img/new_fleet_button.png

#. Filling out the form!

   .. image:: img/new_fleet_form.png

Here's a quick run down of what the form fields mean:

Name
    The name of the fleet to help you keep them organized. Players won't see these names.

Cluster
    The cluster of physics servers the Fleet's Game Servers should run in.

Build
    The name of the game build to run on the Fleet's Game Servers.

Port
    The port that your game listens for connections on.

Labels
    Arbitrary key/value pairs which can be used to select which Game Server should get allocated for a lobby.

    For example, if your Fleet has labels like::

        {
            "game-mode": "battle-royale"
        }

    Then a lobby can be made to select Game Servers with this label by having lobby properties like::

        {
            "gameServerSelectors": [
                {
                    "matchLabels": {
                        "game-mode": "battle-royale"
                    }
                }
            ]
        }

Min Replicas
    The minimum number of Game Servers to keep ready.

Max Replicas
    The maximum number of Game Servers to keep ready.

Buffer Size
    How many extra Game Servers to keep ready beyond the ones already allocated.

Autoscaling Interval
    How often (in seconds) to count the number of Game Servers and launch or shutdown some according to the rules above.

Monitoring your Fleets
----------------------

TODO: show how to keep an eye on your fleets and game servers in the dashboard
